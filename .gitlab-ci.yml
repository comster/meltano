image: python:3.6
variables:
  PIP_CACHE_DIR: "$CI_PROJECT_DIR/.cache"

include:
  - "/orchestrate/singer/build-ci.yml"

stages:
    - test
    - publish
    - build_base
    - build
    - review
    - production
    - dockerhub

cache:
  paths:
    - .cache/pip
    - venv/

.pipenv_install: &pipenv_install
  - pip install pipenv
  - pipenv run python --version
  - pipenv run pip install cython
  - pipenv install --dev
  - pipenv run pip install -e .

.lint:
  stage: test
  before_script: *pipenv_install
  script:
  - pipenv run flake8
  allow_failure: true

.test:
  stage: test
  before_script: *pipenv_install
  script:
  - pipenv run pytest 
  allow_failure: true
 
.run:
  script:
  - python setup.py bdist_wheel
  # an alternative approach is to install and run:
  - pip install dist/*
  # run the command here
  artifacts:
    paths:
    - dist/*.whl

.pages:
  script:
  - pip install sphinx sphinx-rtd-theme
  - cd doc ; make html
  - mv build/html/ ../public/
  artifacts:
    paths:
    - public
  only:
  - master

publish:
  stage: publish
  before_script:
  - pip install twine 
  script:
  - python setup.py sdist
  - twine upload dist/*
  only:
  - master
  - dev
  when: manual

.docker_build: &docker_build
  image: docker:latest
  stage: build
  variables:
    DOCKERFILE: .
    DOCKER_DRIVER: overlay2
  services:
  - docker:dind
  script:
  - export DOCKER_IMAGE_NAME=${IMAGE_NAME:-$CI_PROJECT_SLUG}
  - export DOCKER_IMAGE_TAG=${IMAGE_TAG:-$CI_COMMIT_REF_NAME}
  - docker login -u gitlab-ci-token -p $CI_JOB_TOKEN $CI_REGISTRY
  - docker build -t $CI_REGISTRY_IMAGE/$DOCKER_IMAGE_NAME:$DOCKER_IMAGE_TAG -f $DOCKERFILE .
  - docker push $CI_REGISTRY_IMAGE/$DOCKER_IMAGE_NAME:$DOCKER_IMAGE_TAG

.dev_build_meltano:
  <<: *docker_build
  image: docker:latest
  variables:
    DOCKER_DRIVER: overlay2
    IMAGE_NAME: meltano
    DOCKERFILE: ./docker/Dockerfile
  except:
  - master

.build_meltano:
  <<: *docker_build
  variables:
    DOCKER_DRIVER: overlay2
    IMAGE_NAME: meltano
    IMAGE_TAG: latest
    DOCKERFILE: ./docker/Dockerfile
  only:
  - master

.review:
  image: silvs/kubectl:latest
  stage: review
  script:
  - echo ""
  only:
  - branches
  except:
  - master

meltano_base_hub:
  image: docker:latest
  stage: build_base
  variables:
    DOCKER_DRIVER: overlay2
  services:
  - docker:dind
  script:
  - docker login -u $DOCKER_HUB_USER -p $DOCKER_HUB_PASSWORD
  - docker build -t meltano/meltano:base-v$CI_COMMIT_REF_NAME -f app/images/base.Dockerfile .
  - docker push meltano/meltano:base-v$CI_COMMIT_REF_NAME
  only:
  - tags

meltano_full_hub:
  image: docker:latest
  stage: build
  variables:
    DOCKER_DRIVER: overlay2
  services:
  - docker:dind
  script:
  - docker login -u $DOCKER_HUB_USER -p $DOCKER_HUB_PASSWORD
  - docker build -t meltano/meltano:full-v$CI_COMMIT_REF_NAME -f app/images/full.Dockerfile .
  - docker push meltano/meltano:full-v$CI_COMMIT_REF_NAME
  only:
  - tags

meltano_base:
  image: docker:latest
  stage: build_base
  variables:
    DOCKER_DRIVER: overlay2
  services:
  - docker:dind
  script:
  - docker login -u gitlab-ci-token -p $CI_JOB_TOKEN $CI_REGISTRY
  - docker build -t $CI_REGISTRY_IMAGE/meltano:base-$CI_COMMIT_SHA -f app/images/base.Dockerfile .
  - docker push $CI_REGISTRY_IMAGE/meltano:base-$CI_COMMIT_SHA
  only:
  - branches

meltano_full:
  image: docker:latest
  stage: build
  variables:
    DOCKER_DRIVER: overlay2
  services:
  - docker:dind
  script:
  - sed -i "2s?.*?FROM $CI_REGISTRY_IMAGE/meltano:base-$CI_COMMIT_SHA?" app/images/full.Dockerfile
  - docker login -u gitlab-ci-token -p $CI_JOB_TOKEN $CI_REGISTRY
  - docker build -t $CI_REGISTRY_IMAGE/meltano:full-$CI_COMMIT_SHA -f app/images/full.Dockerfile .
  - docker push $CI_REGISTRY_IMAGE/meltano:full-$CI_COMMIT_SHA
  only:
  - branches

production:
  image: silvs/kubectl:latest
  stage: production
  environment:
    name: production
    url: https://melt.gitlab-bizops.com
  script:
  - cd app/chart/meltano
  - helm init --client-only
  - helm dep update
  - helm upgrade --install --namespace default meltano . --set image.repository=$CI_REGISTRY_IMAGE/meltano,image.tag=base-$CI_COMMIT_SHA,ingress.enabled=true,'global.ingress.hosts[0]=melt.gitlab-bizops.com','global.ingress.tls[0].hosts[0]=melt.gitlab-bizops.com','global.ingress.tls[0].secretName=melt-tls',postgresql.postgresPassword=$POSTGRES_PASSWORD,oauth2-proxy.clientId=$OAUTH2_PROXY_CLIENT_ID,oauth2-proxy.clientSecret=$OAUTH2_PROXY_CLIENT_SECRET,oauth2-proxy.cookieSecret=$OAUTH2_PROXY_COOKIE_SECRET,oauth2-proxy.enabled=true,global.oauth2-proxy.enabled=true,oauth2-proxy.emailDomain=gitlab.com,oauth2-proxy.gitlabHost=https://dev.gitlab.org
  only:
  - master